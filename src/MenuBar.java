import javax.swing.*;

public class MenuBar {

	public static void main(String[] args) {
		MenuFrame2 miMarco = new MenuFrame2();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

}
class MenuFrame2 extends JFrame {
	public MenuFrame2() {
		setBounds(500, 200, 400, 400);
		MenuLamina2 miLamina = new MenuLamina2();
		add(miLamina);
		setVisible(true);
	}
}
class MenuLamina2 extends JPanel {
	public MenuLamina2() {
		JMenuBar miBarra = new JMenuBar();
		JMenu archivo = new JMenu("Archivo");
		JMenu edicion = new JMenu("Edicion");
		JMenu herramientas = new JMenu("Herramientas");
		JMenu opciones = new JMenu("Opciones");
		JMenuItem guardarComo = new JMenuItem("Guardar como...", new ImageIcon("bin\\images2\\save.png"));
		guardarComo.setHorizontalTextPosition(SwingConstants.RIGHT);
		JMenuItem guardar = new JMenuItem("Guardar", new ImageIcon("bin\\images2\\save.png"));
		guardar.setHorizontalTextPosition(SwingConstants.RIGHT);
		JMenuItem salir = new JMenuItem("Salir", new ImageIcon("bin\\images2\\save.png"));
		salir.setHorizontalTextPosition(SwingConstants.RIGHT);
		JMenuItem cortar = new JMenuItem("Cortar", new ImageIcon("bin\\images2\\cut.png"));
		cortar.setHorizontalTextPosition(SwingConstants.RIGHT);
		JMenuItem copiar = new JMenuItem("Copiar", new ImageIcon("bin\\images2\\copy.png"));
		copiar.setHorizontalTextPosition(SwingConstants.RIGHT);
		JMenuItem pegar = new JMenuItem("Pegar", new ImageIcon("bin\\images2\\paste.png"));
		pegar.setHorizontalTextPosition(SwingConstants.RIGHT);
		
		JMenuItem generales = new JMenuItem("Generales");
		////////////////
		JPopupMenu emergente = new JPopupMenu();
		JMenuItem opcion1e = new JMenuItem("Opcion 1");
		JMenuItem opcion2e = new JMenuItem("Opcion 2");
		JMenuItem opcion3e = new JMenuItem("Opcion 3");
		emergente.add(opcion1e);
		emergente.add(opcion2e);
		emergente.add(opcion3e);
		this.setComponentPopupMenu(emergente);
		/////////////
		JToolBar barra = new JToolBar();
		
		
		add(miBarra);
		miBarra.add(archivo);
		miBarra.add(edicion);
		miBarra.add(herramientas);
		miBarra.add(opciones);		
		archivo.add(guardar);
		archivo.add(guardarComo);
		archivo.add(salir);
		edicion.add(cortar);
		edicion.add(copiar);
		edicion.add(pegar);
		
	}
}