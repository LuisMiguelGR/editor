
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.*;
import javax.swing.text.rtf.RTFEditorKit;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import sun.tools.jar.resources.jar;

public class EditorTextoV1 {

	public static void main(String[] args) {
		MarcoPrincipal meuMarco = new MarcoPrincipal();
	}

}

class MarcoPrincipal extends JFrame {

	public MarcoPrincipal() {
		this.setExtendedState(MAXIMIZED_BOTH);
		this.setMinimumSize(new Dimension(450, 250));
		this.setTitle("JavaWord");
		this.setIconImage(new ImageIcon("bin"+File.separator+"images"+File.separator+"logo.png").getImage());
		this.add(new LaminaPrincipal());
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				int respuesta = JOptionPane.showConfirmDialog(null, "¿Desexa realmente cerrar?", "Cerrar aplicacion...", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				if (respuesta == 1) { setDefaultCloseOperation(0);
				} else { System.exit(0); }
			}
		});
		this.setVisible(true);
		
	}
}

class LaminaPrincipal extends JPanel {
	// Menu Principal
	JMenu archivo;
	JMenu fonte;
	JMenu estilo;
	JMenu tamanho;
	// Items Arquivo
	JMenuItem abrir;
	JMenuItem gardar;
	JMenuItem gardarComo;
	JMenuItem sair;
	// Items Fonte

	// Items Estilo
	JMenuItem negrita;
	JMenuItem cursiva;
	JMenuItem sublinhado;
	// Items Tamaño
	ButtonGroup grupo;
	JRadioButton tam12;
	JRadioButton tam16;
	JRadioButton tam20;
	JRadioButton tam24;
	// Label path
	JLabel pathDestino;
	// TEXTO
	JTextPane texto;
	// BarraTareas
	JButton botonNegrita;
	JButton botonCursiva;
	JButton botonSublinhado;
	JButton botonCorTexto;
	JButton botonAlignIzq;
	JButton botonAlignDer;
	JButton botonAlignJus;
	JButton botonAlignCen;
	// Color
	Color cor = Color.black;
	// File
	JFileChooser mipath;
	//editor
	RTFEditorKit editor;
	//Archivo
	File archivoRtf;
	//boolean
	boolean estaModificado;

	public LaminaPrincipal() {
		estaModificado = false;
		this.setLayout(new BorderLayout());
		this.add(new LaminaSuperior(), BorderLayout.NORTH);
		this.add(new LaminaCentro(), BorderLayout.CENTER);
	}

	class LaminaSuperior extends JPanel {

		public LaminaSuperior() {
			this.setLayout(new GridLayout(1, 2));
			JMenuBar barraMenus = new JMenuBar();
			this.add(barraMenus);
			// Arquivo
			archivo = new JMenu("Arquivo");
			abrir = new JMenuItem("Abrir");
			archivo.add(abrir);
			abrir.addActionListener(new AbrirListener());
			gardar = new JMenuItem("Gardar");
			archivo.add(gardar);
			gardar.addActionListener(new GardarListener());
			gardar.setActionCommand("GARDAR");
			gardarComo = new JMenuItem("Gardar como");
			archivo.add(gardarComo);
			gardarComo.addActionListener(new GardarListener());
			//gardarComo.addActionListener(new GardarComoListener());
			gardarComo.setActionCommand("GARDARCOMO");
			sair = new JMenuItem("Sair");
			archivo.add(sair);
			sair.addActionListener(new SairListener());
			barraMenus.add(archivo);
			// Fonte
			fonte = new JMenu("Fonte");
			String[] fontArray = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
			for (String tipoFont : fontArray) {
				JMenuItem font = new JMenuItem(tipoFont);
				fonte.add(font);
				font.addActionListener(new StyledEditorKit.FontFamilyAction("", tipoFont));
			}
			barraMenus.add(fonte);
			MenuScroller.setScrollerFor(fonte, 8, 125, 3, 1);
			// Estilo			
			estilo = new JMenu("Estilo");
			negrita = new JMenuItem("Negrita");			
			estilo.add(negrita);
			negrita.addActionListener(new StyledEditorKit.BoldAction());
			cursiva = new JMenuItem("Cursiva");
			KeyStroke ctrlK = KeyStroke.getKeyStroke("control k");
			cursiva.setAccelerator(ctrlK);
			estilo.add(cursiva);
			cursiva.addActionListener(new StyledEditorKit.ItalicAction());
			sublinhado = new JMenuItem("Subliñado");
			KeyStroke ctrlS = KeyStroke.getKeyStroke("control s");
			sublinhado.setAccelerator(ctrlS);
			estilo.add(sublinhado);
			sublinhado.addActionListener(new StyledEditorKit.UnderlineAction());
			barraMenus.add(estilo);
			//KeyStrokes
			negrita.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
			cursiva.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
			sublinhado.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
			// Tama�o
			tamanho = new JMenu("Tamaño");
			grupo = new ButtonGroup();
			tam12 = new JRadioButton("12");
			tam16 = new JRadioButton("16");
			tam20 = new JRadioButton("20");
			tam24 = new JRadioButton("24");
			tam16.setEnabled(false );
			grupo.add(tam12);
			grupo.add(tam16);
			grupo.add(tam20);
			grupo.add(tam24);
			tamanho.add(tam12);
			tam12.addActionListener(new StyledEditorKit.FontSizeAction("", 12));
			tamanho.add(tam16);
			tam16.addActionListener(new StyledEditorKit.FontSizeAction("", 16));
			tamanho.add(tam20);
			tam20.addActionListener(new StyledEditorKit.FontSizeAction("", 20));
			tamanho.add(tam24);
			tam24.addActionListener(new StyledEditorKit.FontSizeAction("", 24));
			barraMenus.add(tamanho);
			// Label path
			mipath = new JFileChooser();
			pathDestino = new JLabel(mipath.getCurrentDirectory().getAbsolutePath()+File.separator+"NovoDocumento.rtf");
			this.add(pathDestino);
		}
	}

	class LaminaCentro extends JPanel {

		public LaminaCentro() {
			this.setLayout(new BorderLayout());
			texto = new JTextPane();
			texto.setEditable(true);
			editor = new RTFEditorKit();
			texto.setEditorKit(editor);
			JScrollPane scroll = new JScrollPane(texto);
			this.add(scroll, BorderLayout.CENTER);
			texto.addKeyListener(new textoListener());
			
                        
			// Creacion ToolBar
			JToolBar barraHerramientas = new JToolBar(JToolBar.VERTICAL);
			botonNegrita = new JButton(new ImageIcon(getClass().getResource("images"+File.separator+"negrita.png")));
			barraHerramientas.add(botonNegrita);
			botonNegrita.addActionListener(new StyledEditorKit.BoldAction());
			botonCursiva = new JButton(new ImageIcon(getClass().getResource("images"+File.separator+"cursiva.png")));
			barraHerramientas.add(botonCursiva);
			botonCursiva.addActionListener(new StyledEditorKit.ItalicAction());
			botonSublinhado = new JButton(new ImageIcon(getClass().getResource("images"+File.separator+"subrayado.png")));
			barraHerramientas.add(botonSublinhado);
			botonSublinhado.addActionListener(new StyledEditorKit.UnderlineAction());
			botonCorTexto = new JButton(new ImageIcon(getClass().getResource("images"+File.separator+"text_color.png")));
			barraHerramientas.add(botonCorTexto);
			botonCorTexto.addActionListener(new CorListener());
			botonAlignIzq = new JButton(new ImageIcon(getClass().getResource("images"+File.separator+"izquierda.png")));
			barraHerramientas.add(botonAlignIzq);
			botonAlignIzq.addActionListener(new StyledEditorKit.AlignmentAction("", StyleConstants.ALIGN_LEFT));
			botonAlignDer = new JButton(new ImageIcon(getClass().getResource("images"+File.separator+"derecha.png")));
			barraHerramientas.add(botonAlignDer);
			botonAlignDer.addActionListener(new StyledEditorKit.AlignmentAction("", StyleConstants.ALIGN_RIGHT));
			botonAlignJus = new JButton(new ImageIcon(getClass().getResource("images"+File.separator+"justificado.png")));
			barraHerramientas.add(botonAlignJus);
			botonAlignJus.addActionListener(new StyledEditorKit.AlignmentAction("", StyleConstants.ALIGN_JUSTIFIED));
			botonAlignCen = new JButton(new ImageIcon(getClass().getResource("images"+File.separator+"centrado.png")));
			barraHerramientas.add(botonAlignCen);
			botonAlignCen.addActionListener(new StyledEditorKit.AlignmentAction("", StyleConstants.ALIGN_CENTER));
			this.add(barraHerramientas, BorderLayout.WEST);
			//Informacion adicional botones			
			botonNegrita.setToolTipText("Negrita");
			botonCursiva.setToolTipText("Cursiva");
			botonSublinhado.setToolTipText("Subrayado");
			botonCorTexto.setToolTipText("Color");
			botonAlignIzq.setToolTipText("Alineado Izquierdo");
			botonAlignDer.setToolTipText("Alineado Derecho");
			botonAlignCen.setToolTipText("Alineado Centrado");
			botonAlignCen.setToolTipText("Justificado");
		}
	}

	// Listeners
	private class CorListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			cor = JColorChooser.showDialog(null, "Cor do texto", cor);
			if (cor == null) {
				cor = Color.black;
			}
			SimpleAttributeSet atributo = new SimpleAttributeSet();
			StyleConstants.setForeground(atributo, cor);
			texto.setCharacterAttributes(atributo, false);
		}
	}

	private class AbrirListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			JFileChooser archivo = new JFileChooser();
			archivo.showOpenDialog(null);
			try {
				editor.read(new FileInputStream(archivo.getSelectedFile()), texto.getStyledDocument(), 0);
			} catch (IOException | BadLocationException e2) {
				e2.printStackTrace();
			}
			pathDestino.setText(archivo.getCurrentDirectory().getPath() + File.separator
					+ archivo.getName(archivo.getSelectedFile()));
			/*pathDestino.setText(archivo.getCurrentDirectory().getPath() + File.separator
					+ archivo.getName(archivo.getSelectedFile()));
			*/
		}

	}

	private class GardarListener implements ActionListener {
		
		private FileNameExtensionFilter filtroRtf = new FileNameExtensionFilter("*.rtf", ".rtf");
		//FileNameExtensionFilter filtroDoc = new FileNameExtensionFilter("*.doc", ".doc");
		private JFileChooser archivo;
		
		private void guardarComo() {
			archivo = new JFileChooser(); 
			//archivo.setFileFilter(filtroDoc);
			archivo.setFileFilter(filtroRtf);
			int val = archivo.showSaveDialog(null);
			if(val == JFileChooser.APPROVE_OPTION) {
				archivoRtf = new File(archivo.getSelectedFile().getAbsolutePath()+".rtf");
				try {
					editor.write(new FileOutputStream(archivoRtf), texto.getDocument(), 0, texto.getDocument().getLength());
				} catch (FileNotFoundException e2) {
					e2.printStackTrace();
				} catch (IOException e2) {
					e2.printStackTrace();
				} catch (BadLocationException e2) {
					e2.printStackTrace();
				}
				pathDestino.setText(archivo.getCurrentDirectory().getAbsolutePath()+File.separator+archivo.getSelectedFile().getName()+".rtf");
			}
			
			
			
		}
		
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand()=="GARDAR") {
				if(estaModificado==true) {
					guardarComo();
					estaModificado=false;
				}else {
					try {
						editor.write(new FileOutputStream(pathDestino.getText()), texto.getDocument(), 0, texto.getDocument().getLength());
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (BadLocationException e1) {
						e1.printStackTrace();
					}
					estaModificado=false;
				}
			}else if(e.getActionCommand()=="GARDARCOMO") {
				guardarComo();
				estaModificado=false;
			}

		}

	}

	
	
	private class GardarComoListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0)  {
			FileNameExtensionFilter filtroRtf = new FileNameExtensionFilter("*.rtf", ".rtf");
			//FileNameExtensionFilter filtroDoc = new FileNameExtensionFilter("*.doc", ".doc");
			JFileChooser archivo = new JFileChooser(); 
			//archivo.setFileFilter(filtroDoc);
			archivo.setFileFilter(filtroRtf);
			archivo.showSaveDialog(null);
			archivoRtf = new File(archivo.getSelectedFile().getAbsolutePath()+".rtf");
			
			try {
				editor.write(new FileOutputStream(archivoRtf), texto.getDocument(), 0, texto.getDocument().getLength());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			pathDestino.setText(archivo.getCurrentDirectory().getAbsolutePath()+File.separator+archivo.getSelectedFile().getName()+".rtf");
		}
		/*
		  FileNameExtensionFilter filtro = new FileNameExtensionFilter("","rtf");
		  JFileChooser archivo = new JFileChooser(); archivo.showSaveDialog(null);
		  archivo.setFileFilter(filtro); 
		  File archivoFile = archivo.getSelectedFile();
		  try { 
		  BufferedWriter buffSalida = new BufferedWriter(new FileWriter(archivoFile)); texto.write(buffSalida); buffSalida.close(); 
		  }
		  catch (IOException e) { 
		  e.printStackTrace(); 
		  }
		  pathDestino.setText(archivo.getCurrentDirectory().getPath()+File.separator+archivo.getName(archivo.getSelectedFile()));
		 */
			 
	}

	private class SairListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int respuesta = JOptionPane.showConfirmDialog(null, "¿Realmente desexa sair?", "Confirmar sair", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (respuesta==0) {
				System.exit(0);
			} 
		}

	}
	private class textoListener implements KeyListener {

		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			estaModificado=true;
		}
		
	}
}